> ✍️作者简介：小北编程（专注于HarmonyOS、Android、Java、Web、TCP/IP等技术方向）
🐳博客主页： [开源中国](https://my.oschina.net/xtlu)、[稀土掘金](https://juejin.cn/user/143390264268079)、[51cto博客](https://blog.51cto.com/u_9019776)、[博客园](https://i.cnblogs.com/articles)、[知乎](https://www.zhihu.com/people/mo-xiao-bei-38)、[简书](https://www.jianshu.com/u/1a7405769a34)、[慕课网](https://www.imooc.com/u/index/allcourses)、[CSDN](https://blog.csdn.net/u014696856?spm=1011.2124.3001.5343)   
🔔如果文章对您有一定的帮助请👉关注✨、点赞👍、收藏📂、评论💬。  
🔥如需转载请参考[【转载须知】](https://blog.csdn.net/u014696856/article/details/131813336?spm=1001.2014.3001.5501)

@[TOC]



今天我想与大家分享一些在播放器开发中常用的音视频流地址，涵盖了纯音频、纯视频以及各种常见封装格式，比如rtmp、m3u8、flv等。在网络上，常见的测试地址往往已经失效，为了解决这个问题，我打算在这里创建一个播放列表。如果你有可用的音视频地址，请不吝留言分享给我，不胜感激！🙏

## 各种形式测试地址
| 描述 | 媒体地址 | 状态 |
|-----|-------------|------|
| 1080P、60fps | https://sdk-release.qnsdk.com/1080_60_5390.mp4 | <font color="green">可用</font> |
| 2K、25fps | https://sdk-release.qnsdk.com/2K_25_11700.mp4 | <font color="green">可用</font> |
| 2K、60fps | https://sdk-release.qnsdk.com/2K_60_6040.mp4 | <font color="green">可用</font> |
| 4K、25fps | https://sdk-release.qnsdk.com/4K_25_21514.mp4 | <font color="green">可用</font> |
| 带有端口号的视频 | https://img.qunliao.info:443/4oEGX68t_9505974551.mp4 | <font color="green">可用</font> |
| h265资源 | http://demo-videos.qnsdk.com/bbk-H265-50fps.mp4 | <font color="green">可用</font> |
| VR视频 | http://demo-videos.qnsdk.com/VR-Panorama-Equirect-Angular-4500k.mp4 | <font color="green">可用</font> |
| 含有旋转方向的视频 | https://sdk-release.qnsdk.com/VID_20220207_144828.mp4 | <font color="green">可用</font> |
| 带有封面的mp3音频流 | https://sdk-release.qnsdk.com/1599039859854_9242359.mp3 | <font color="green">可用</font> |
| 非正常比例媒体流 | https://sdk-release.qnsdk.com/10037108_065355-hd%20%281%29.mp4 | <font color="green">可用</font> |
| 1080p 60fps 纯视频 | http://demo-videos.qnsdk.com/only-video-1080p-60fps.m4s | <font color="green">可用</font> |
| flv 格式资源 韩国 枪花乐队 《Don't Cry》 | https://sdk-release.qnsdk.com/flv.flv | <font color="green">可用</font> |


![20210729105337ce32.gif](https://image-tt-private.toutiao.com/tos-cn-i-3003/0d40f7e370de4c0482ddafda05d335d8~tplv-obj.image?lk3s=993df49e&policy=eyJ2bSI6MywidWlkIjoiMTEwOTUyMDA5MjU1In0%3D&traceid=20240103164351CA6837F48BF02795177F&x-orig-authkey=5a21e4afda5945d9a206a695e4c78a63&x-orig-expires=2147483647&x-orig-sign=0YrFf15a%2BlfmwpYt5MTZrQZJvjw%3D)

## 纯音频综合地址
| 描述 | 媒体地址 | 状态 |
|-----|-------------|------|
| 纯音频、amr格式 | https://sdk-release.qnsdk.com/amr.amr | <font color="green">可用</font> |
| 纯音频、ogg格式 | https://sdk-release.qnsdk.com/ogg.ogg | <font color="green">可用</font> |
| 纯音频、ogg格式 | https://sdk-release.qnsdk.com/aac.aac | <font color="green">可用</font> |
| 纯音频、ac3格式 | https://sdk-release.qnsdk.com/ac3.ac3 | <font color="green">可用</font> |
| 纯音频 | https://sdk-release.qnsdk.com/ape.ape | <font color="green">可用</font> |
| 纯音频、flac格式 | https://sdk-release.qnsdk.com/flac.flac | <font color="green">可用</font> |
| 纯音频、mp4格式 | https://sdk-release.qnsdk.com/mp4.mp4 | <font color="green">可用</font> |
| 纯音频、wav格式 | https://sdk-release.qnsdk.com/wav.wav | <font color="green">可用</font> |
| 纯音频、caf格式 | https://sdk-release.qnsdk.com/caf.caf | <font color="green">可用</font> |
| 纯音频、aiff格式 | https://sdk-release.qnsdk.com/aiff.aiff | <font color="green">可用</font> |
| 纯音频、m4r格式 | https://sdk-release.qnsdk.com/m4r.m4r | <font color="green">可用</font> |

![20210729105337ce32.gif](https://image-tt-private.toutiao.com/tos-cn-i-3003/0d40f7e370de4c0482ddafda05d335d8~tplv-obj.image?lk3s=993df49e&policy=eyJ2bSI6MywidWlkIjoiMTEwOTUyMDA5MjU1In0%3D&traceid=20240103164351CA6837F48BF02795177F&x-orig-authkey=5a21e4afda5945d9a206a695e4c78a63&x-orig-expires=2147483647&x-orig-sign=0YrFf15a%2BlfmwpYt5MTZrQZJvjw%3D)

## M3U8 测试地址
| 描述 | 媒体地址 | 状态 |
|-----|-------------|------|
| bipbop advanced 22.050Hz stereo @ 40 kbps | http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_16x9/gear0/prog_index.m3u8 | <font color="green">可用</font> |
| bipbop basic master playlist | http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/bipbop_4x3_variant.m3u8 | <font color="green">可用</font> |
| bipbop basic 400x300 @ 232 kbps | http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear1/prog_index.m3u8 | <font color="green">可用</font> |
| bipbop basic 640x480 @ 650 kbps | http://devimages.apple.com.edgekey.net/streaming/examples/bipbop_4x3/gear2/prog_index.m3u8 | <font color="green">可用</font> |
| 漳浦综合HD | http://220.161.87.62:8800/hls/0/index.m3u8 | <font color="green">可用</font> |
| 巴基斯坦360TV | http://cdn3.toronto360.tv:8081/toronto360/hd/playlist.m3u8 | <font color="green">可用</font> |
| 埃及电视台 | https://svs.itworkscdn.net/kingdomsatlive/kingdomsat.smil/playlist_dvr.m3u8 | <font color="green">可用</font> |
| 大熊兔  | https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8 | <font color="green">可用</font> |
| 实时 Akamai  | https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8 | <font color="green">可用</font> |
| 钢铁之泪 | https://demo.unified-streaming.com/k8s/features/stable/video/tears-of-steel/tears-of-steel.ism/.m3u8 | <font color="green">可用</font> |
| fMP4 计时器 |https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8 | <font color="green">可用</font> |

![20210729105337ce32.gif](https://image-tt-private.toutiao.com/tos-cn-i-3003/0d40f7e370de4c0482ddafda05d335d8~tplv-obj.image?lk3s=993df49e&policy=eyJ2bSI6MywidWlkIjoiMTEwOTUyMDA5MjU1In0%3D&traceid=20240103164351CA6837F48BF02795177F&x-orig-authkey=5a21e4afda5945d9a206a695e4c78a63&x-orig-expires=2147483647&x-orig-sign=0YrFf15a%2BlfmwpYt5MTZrQZJvjw%3D)

## RTMP 测试地址
| 描述 | 媒体地址 | 状态 |
|-----|-------------|------|
| 美国链接1 | rtmp://ns8.indexforce.com/home/mystream | <font color="green">可用</font> |
| 美国链接2 | rtmp://media3.scctv.net/live/scctv_800 | <font color="green">可用</font> |
| 韩国GoodTV | rtmp://mobliestream.c3tv.com:554/live/goodtv.sdp | <font color="green">可用</font> |
| 伊拉克 Al Sharqiya 电视台 | rtmp://ns8.indexforce.com/home/mystream | <font color="green">可用</font> |
| 邓紫棋 多美丽mv | rtmp://liteavapp.qcloud.com/live/liteavdemoplayerstreamid | <font color="green">可用</font> |

特别推荐**邓紫棋 多美丽mv**秒开不卡顿

✍️写作不易，如果文章对您有些许的帮助请帮忙点赞👍收藏📂您的支持是我写下去的动力💪


<font color=#198632>无论是哪个阶段，坚持努力都是成功的关键。</font><font color=#0e5cca9>不要停下脚步，继续前行，即使前路崎岖，也请保持乐观和勇气。</font><font color=#9eabf0>相信自己的能力，你所追求的目标定会在不久的将来实现。</font><font color=#ff0000>加油！</font>