
## 一、工具类

| 分类         | 名称                               | 介绍                           | 资源地址                                           |
|------------|----------------------------------|------------------------------|----------------------------------------------------|
| 1.1、图片加载   | Glide                            | 最常用的图片加载工具                   | [https://gitee.com/HarmonyOS-TPC/glide](https://gitee.com/HarmonyOS-TPC/glide)                |
|            | Glide Transformations            | 基于Glide的图片变化库                | [https://gitee.com/HarmonyOS-TPC/glide-transformations](https://gitee.com/HarmonyOS-TPC/glide-transformations) |
|            | Fresco                           | Facebook出品的图片加载工具            | [https://gitee.com/HarmonyOS-TPC/fresco](https://gitee.com/HarmonyOS-TPC/fresco)               |
|            | Picasso                          | 常用的图片加载工具之一                  | [https://gitee.com/HarmonyOS-TPC/picasso](https://gitee.com/HarmonyOS-TPC/picasso)              |
|            | ohos-gif-drawable                | GIF图片加载工具                    | [https://gitee.com/HarmonyOS-TPC/ohos-gif-drawable](https://gitee.com/HarmonyOS-TPC/ohos-gif-drawable)    |
|            | ohos-Universal-Image-Loader      | 常用图片加载工具之一                   | [https://gitee.com/HarmonyOS-TPC/ohos-Universal-Image-Loader](https://gitee.com/HarmonyOS-TPC/ohos-Universal-Image-Loader) |
|            | Keyframes                        | 基于Adobe矢量图动画加载               | [https://gitee.com/HarmonyOS-TPC/Keyframes](https://gitee.com/HarmonyOS-TPC/Keyframes)            |
|            | Ion                              | 图片加载工具                       | [https://gitee.com/HarmonyOS-TPC/ion](https://gitee.com/HarmonyOS-TPC/ion)                 |
| 1.2、数据封装传递 | EventBus                         | 最常用的消息传递工具                   | [https://gitee.com/HarmonyOS-TPC/EventBus](https://gitee.com/HarmonyOS-TPC/EventBus)            |
|            | Rxohos                           | 基于RxJava                     | [https://gitee.com/HarmonyOS-TPC/Rxohos](https://gitee.com/HarmonyOS-TPC/Rxohos)              |
|            | RxBus                            | 基于RxJava的消息传递工具              | [https://gitee.com/HarmonyOS-TPC/RxBus](https://gitee.com/HarmonyOS-TPC/RxBus)               |
|            | Otto                             | 基于Guava的消息传递工具               | [https://gitee.com/HarmonyOS-TPC/otto](https://gitee.com/HarmonyOS-TPC/otto)                |
|            | RxLifeCycle                      | 基于RxJava的生命周期获取              | [https://gitee.com/HarmonyOS-TPC/RxLifeCycle](https://gitee.com/HarmonyOS-TPC/RxLifeCycle)         |
|            | RxBinding                        | 基于控件的扩展                      | [https://gitee.com/HarmonyOS-TPC/RxBinding](https://gitee.com/HarmonyOS-TPC/RxBinding)           |
|            | Agera                            | 数据交互                         | [https://gitee.com/HarmonyOS-TPC/agera](https://gitee.com/HarmonyOS-TPC/agera)               |
|            | Anadea_RxBus                     | 基于RxJava的消息传递工具              | [https://gitee.com/HarmonyOS-TPC/Anadea_RxBus](https://gitee.com/HarmonyOS-TPC/Anadea_RxBus)        |
|            | LoadSir                          | 注册事件进行回调操作                   | [https://gitee.com/HarmonyOS-TPC/LoadSir](https://gitee.com/HarmonyOS-TPC/LoadSir)             |
| 1.3、日志     | Logger                           | Log工具                        | [https://gitee.com/HarmonyOS-TPC/logger](https://gitee.com/HarmonyOS-TPC/logger)             |
|            | xLog                             | 日志工具                         | [https://gitee.com/HarmonyOS-TPC/xLog](https://gitee.com/HarmonyOS-TPC/xLog)               |
| 1.4、权限相关   | xxpermissions                    | 权限申请                         | [https://gitee.com/HarmonyOS-TPC/xxpermissions](https://gitee.com/HarmonyOS-TPC/xxpermissions)  |
|            | PermissionsDispatcher            | 权限申请                         | [https://gitee.com/HarmonyOS-TPC/PermissionsDispatcher](https://gitee.com/HarmonyOS-TPC/PermissionsDispatcher) |
|            | Dexter                           | 权限申请                         | [https://gitee.com/HarmonyOS-TPC/Dexter](https://gitee.com/HarmonyOS-TPC/Dexter)           |
| 1.5、相机/相册  | BGAQRCode-ohos                   | 基于ZXing的二维码扫描工具              | [https://gitee.com/HarmonyOS-TPC/BGAQRCode-ohos](https://gitee.com/HarmonyOS-TPC/BGAQRCode-ohos) |
|            | Matisse                          | 相册访问                         | [https://gitee.com/HarmonyOS-TPC/Matisse](https://gitee.com/HarmonyOS-TPC/Matisse)      |
|            | ImagePicker                      | 相册访问                         | [https://gitee.com/HarmonyOS-TPC/ImagePicker](https://gitee.com/HarmonyOS-TPC/ImagePicker)   |
|            | CameraView                       | 相机使用组件                       | [https://gitee.com/HarmonyOS-TPC/CameraView](https://gitee.com/HarmonyOS-TPC/CameraView)   |
| 1.6、其他     | Butterknife                      | 通过反射调用方法                     | [https://gitee.com/HarmonyOS-TPC/butterknife](https://gitee.com/HarmonyOS-TPC/butterknife)  |
|            | assertj-ohos                     | 快速调用其他封装库                    | [https://gitee.com/HarmonyOS-TPC/assertj-ohos](https://gitee.com/HarmonyOS-TPC/assertj-ohos) |
|            | ohos-utilset                     | 工具集                          | [https://gitee.com/HarmonyOS-TPC/ohos-utilset](https://gitee.com/HarmonyOS-TPC/ohos-utilset) |
|            | xUtils3                          | 工具集 包含网络，图片，控件等              | [https://gitee.com/HarmonyOS-TPC/xUtils3](https://gitee.com/HarmonyOS-TPC/xUtils3)      |
|            | device-year-class                | 获取手机年份                       | [https://gitee.com/HarmonyOS-TPC/device-year-class](https://gitee.com/HarmonyOS-TPC/device-year-class) |
|            | swipe                            | 对于手势封装应用                     | [https://gitee.com/HarmonyOS-TPC/swipe](https://gitee.com/HarmonyOS-TPC/swipe)       |
|            | TinyPinyin                       | 文字转拼音工具                      | [https://gitee.com/HarmonyOS-TPC/TinyPinyin](https://gitee.com/HarmonyOS-TPC/TinyPinyin)  |


### 二、网络类

| 名称                             | 介绍                                    | 资源地址                                           |
|----------------------------------|-----------------------------------------|----------------------------------------------------|
| PersistentCookieJar              | 基于OkHttp3实现的Cookie网络优化           | [https://gitee.com/HarmonyOS-TPC/PersistentCookieJar](https://gitee.com/HarmonyOS-TPC/PersistentCookieJar) |
| Chuck                            | OkHttp本地Client                         | [https://gitee.com/HarmonyOS-TPC/chuck](https://gitee.com/HarmonyOS-TPC/chuck)                     |
| Google HTTP Java Client           | Google HTTP Client库                     | [https://gitee.com/HarmonyOS-TPC/google-http-java-client](https://gitee.com/HarmonyOS-TPC/google-http-java-client) |
| Ohos Async HTTP                   | 基于Apache的HttpClient库构建的Http Client | [https://gitee.com/HarmonyOS-TPC/ohos-async-http](https://gitee.com/HarmonyOS-TPC/ohos-async-http)     |
| OkHttp-OkGo                       | 基于OkHttp封装的库                       | [https://gitee.com/HarmonyOS-TPC/okhttp-OkGo](https://gitee.com/HarmonyOS-TPC/okhttp-OkGo)           |
| OhosAsync                         | 异步网络请求                            | [https://gitee.com/HarmonyOS-TPC/ohosAsync](https://gitee.com/HarmonyOS-TPC/ohosAsync)               |
| Fast-Ohos-Networking              | 快速访问                                | [https://gitee.com/HarmonyOS-TPC/Fast-ohos-Networking](https://gitee.com/HarmonyOS-TPC/Fast-ohos-Networking) |
| FileDownloader                    | 文件下载库                              | [https://gitee.com/HarmonyOS-TPC/FileDownloader](https://gitee.com/HarmonyOS-TPC/FileDownloader)     |
| PRDownloader                      | 文件下载库                              | [https://gitee.com/HarmonyOS-TPC/PRDownloader](https://gitee.com/HarmonyOS-TPC/PRDownloader)         |
| Network Connection Class          | 获取网络状态库                           | [https://gitee.com/HarmonyOS-TPC/network-connection-class](https://gitee.com/HarmonyOS-TPC/network-connection-class) |
| ThinDownloadManager               | 文件下载库                              | [https://gitee.com/HarmonyOS-TPC/ThinDownloadManager](https://gitee.com/HarmonyOS-TPC/ThinDownloadManager) |

### 三、文件数据类

| 分类    | 名称          | 介绍                  | 资源地址                                            |
|---------|---------------|-----------------------|-----------------------------------------------------|
| 数据库  | greenDAO       | 最常用的数据库组件     | [https://gitee.com/HarmonyOS-TPC/greenDAO](https://gitee.com/HarmonyOS-TPC/greenDAO)                     |
| 数据库  | Activeohos      | 数据库SQLite封装       | [https://gitee.com/HarmonyOS-TPC/Activeohos](https://gitee.com/HarmonyOS-TPC/Activeohos)                 |
| 数据库  | RushOrm         | 数据库SQLite封装       | [https://gitee.com/HarmonyOS-TPC/RushOrm](https://gitee.com/HarmonyOS-TPC/RushOrm)                       |
| 数据库  | LitePal         | 数据库SQLite封装       | [https://gitee.com/HarmonyOS-TPC/LitePal](https://gitee.com/HarmonyOS-TPC/LitePal)                       |
| Preferences  | rx-preferences     | 基于Preferences封装存储工具  | [https://gitee.com/HarmonyOS-TPC/rx-preferences](https://gitee.com/HarmonyOS-TPC/rx-preferences)         |
| Preferences  | preferencebinder   | 基于Preferences封装存储工具  | [https://gitee.com/HarmonyOS-TPC/preferencebinder](https://gitee.com/HarmonyOS-TPC/preferencebinder)     |


### 四、音视频

| 分类  | 名称            | 介绍         | 资源地址                                           |
|-------|-----------------|--------------|----------------------------------------------------|
| 视频  | jcodec java     | 解码视频      | [https://gitee.com/HarmonyOS-TPC/jcodec](https://gitee.com/HarmonyOS-TPC/jcodec)                   |
| 音频  | soundtouch      | 支持更改声音速度   | [https://gitee.com/HarmonyOS-TPC/soundtouch](https://gitee.com/HarmonyOS-TPC/soundtouch)          |


### 五、动画图形类

| 分类       | 名称                    | 介绍                | 资源地址                                                |
|----------|-------------------------|-------------------|---------------------------------------------------------|
| 5.1 动画   | ohosViewAnimations       | 一款动画的集合的库         | [https://gitee.com/HarmonyOS-TPC/ohosViewAnimations](https://gitee.com/HarmonyOS-TPC/ohosViewAnimations)            |
|          | lottie-ohos              | 让复杂动画轻松实现         | [https://gitee.com/HarmonyOS-TPC/lottie-ohos](https://gitee.com/HarmonyOS-TPC/lottie-ohos)                       |
|          | confetti                 | 模仿雪花飘落的动画         | [https://gitee.com/HarmonyOS-TPC/confetti](https://gitee.com/HarmonyOS-TPC/confetti)                            |
|          | RippleEffect             | 水波纹点击动画           | [https://gitee.com/HarmonyOS-TPC/RippleEffect](https://gitee.com/HarmonyOS-TPC/RippleEffect)                    |
|          | MetaballLoading          | 一个类似元球进度动画效果      | [https://gitee.com/HarmonyOS-TPC/MetaballLoading](https://gitee.com/HarmonyOS-TPC/MetaballLoading)              |
|          | ohos-Spinkit             | 多种基础动画集合          | [https://gitee.com/HarmonyOS-TPC/ohos-Spinkit](https://gitee.com/HarmonyOS-TPC/ohos-Spinkit)                    |
|          | LoadingView              | 多种多样的loading动画集合  | [https://gitee.com/HarmonyOS-TPC/LoadingView](https://gitee.com/HarmonyOS-TPC/LoadingView)                      |
|          | desertplaceholder        | 沙漠中的带动画的场景        | [https://gitee.com/HarmonyOS-TPC/desertplaceholder](https://gitee.com/HarmonyOS-TPC/desertplaceholder)        |
|          | Sequent                  | 各种文字图片呈现动画        | [https://gitee.com/HarmonyOS-TPC/Sequent](https://gitee.com/HarmonyOS-TPC/Sequent)                              |
|          | ohos-Views               | 各种动画的views        | [https://gitee.com/HarmonyOS-TPC/ohos-Views](https://gitee.com/HarmonyOS-TPC/ohos-Views)                        |
|          | BezierMaker              | 简单的贝赛尔曲线绘制方法      | [https://gitee.com/HarmonyOS-TPC/BezierMaker](https://gitee.com/HarmonyOS-TPC/BezierMaker)                    |
|          | WhorlView                | 圆形转圈动画            | [https://gitee.com/HarmonyOS-TPC/WhorlView](https://gitee.com/HarmonyOS-TPC/WhorlView)                        |
| 5.2 图片处理 | SimpleCropView           | 图片裁剪工具            | [https://gitee.com/HarmonyOS-TPC/SimpleCropView](https://gitee.com/HarmonyOS-TPC/SimpleCropView)                |
|          | Luban                   | 图片压缩工具            | [https://gitee.com/HarmonyOS-TPC/Luban](https://gitee.com/HarmonyOS-TPC/Luban)                                    |
|          | TakePhoto               | 拍照图片旋转剪裁          | [https://gitee.com/HarmonyOS-TPC/TakePhoto](https://gitee.com/HarmonyOS-TPC/TakePhoto)                            |
|          | Compressor              | 图片压缩              | [https://gitee.com/HarmonyOS-TPC/Compressor](https://gitee.com/HarmonyOS-TPC/Compressor)                        |
|          | PloyFun                 | 生成三角玻璃图片          | [https://gitee.com/HarmonyOS-TPC/PloyFun](https://gitee.com/HarmonyOS-TPC/PloyFun)                              |
|          | CompressHelper          | 图片压缩              | [https://gitee.com/HarmonyOS-TPC/CompressHelper](https://gitee.com/HarmonyOS-TPC/CompressHelper)              |


### 六、UI-自定义控件

| 分类                | 名称                                   | 介绍                         | 资源地址                                                |
|-------------------|--------------------------------------|----------------------------|---------------------------------------------------------|
| 6.1 Image         | PhotoView                            | 图片缩放查看                     | [https://gitee.com/HarmonyOS-TPC/PhotoView](https://gitee.com/HarmonyOS-TPC/PhotoView)                             |
|                   | CircleImageView                      | 圆形图片                       | [https://gitee.com/HarmonyOS-TPC/CircleImageView](https://gitee.com/HarmonyOS-TPC/CircleImageView)                 |
|                   | RoundedImageView                     | 圆角图片                       | [https://gitee.com/HarmonyOS-TPC/RoundedImageView](https://gitee.com/HarmonyOS-TPC/RoundedImageView)               |
|                   | subsampling-scale-image-view         | 超高清图查看缩放                   | [https://gitee.com/HarmonyOS-TPC/subsampling-scale-image-view](https://gitee.com/HarmonyOS-TPC/subsampling-scale-image-view) |
|                   | ContinuousScrollableImageView        | 带动画播放的Image                | [https://gitee.com/HarmonyOS-TPC/ContinuousScrollableImageView](https://gitee.com/HarmonyOS-TPC/ContinuousScrollableImageView) |
| 6.2 Text          | drawee-text-view                     | 富文本组件                      | [https://gitee.com/HarmonyOS-TPC/drawee-text-view](https://gitee.com/HarmonyOS-TPC/drawee-text-view)                   |
|                   | ReadMoreTextView                     | 点击展开的Text控件                | [https://gitee.com/HarmonyOS-TPC/ReadMoreTextView](https://gitee.com/HarmonyOS-TPC/ReadMoreTextView)                   |
|                   | MaterialEditText                     | 基于MaterialDesign设计的自定义输入框  | [https://gitee.com/HarmonyOS-TPC/MaterialEditText](https://gitee.com/HarmonyOS-TPC/MaterialEditText)                   |
|                   | XEditText                            | 自定义特殊效果输入                  | [https://gitee.com/HarmonyOS-TPC/XEditText](https://gitee.com/HarmonyOS-TPC/XEditText)                               |
| 6.3 Button        | FloatingActionButton                 | 悬浮button                   | [https://gitee.com/HarmonyOS-TPC/FloatingActionButton](https://gitee.com/HarmonyOS-TPC/FloatingActionButton)               |
|                   | circular-progress-button             | 自定义带进度的按钮                  | [https://gitee.com/HarmonyOS-TPC/circular-progress-button](https://gitee.com/HarmonyOS-TPC/circular-progress-button)     |
|                   | progressbutton                       | 带进度的自定义按钮                  | [https://gitee.com/HarmonyOS-TPC/progressbutton](https://gitee.com/HarmonyOS-TPC/progressbutton)                       |
|                   | SwitchButton                         | 仿iOS的开关按钮                  | [https://gitee.com/HarmonyOS-TPC/SwitchButton](https://gitee.com/HarmonyOS-TPC/SwitchButton)                           |
|                   | SlideSwitch                          | 多种样式的开关按钮                  | [https://gitee.com/HarmonyOS-TPC/SlideSwitch](https://gitee.com/HarmonyOS-TPC/SlideSwitch)                             |
| 6.4 ListContainer | FloatingGroupExpandableListView      | 自定义list组件，支持分类带标题          | [https://gitee.com/HarmonyOS-TPC/FloatingGroupExpandableListView](https://gitee.com/HarmonyOS-TPC/FloatingGroupExpandableListView) |
|                   | XRecyclerView                        | 基于ListContainer下拉刷新        | [https://gitee.com/HarmonyOS-TPC/XRecyclerView](https://gitee.com/HarmonyOS-TPC/XRecyclerView)                           |
|                   | PullToZoomInListView                 | 顶部放大List                   | [https://gitee.com/HarmonyOS-TPC/PullToZoomInListView](https://gitee.com/HarmonyOS-TPC/PullToZoomInListView)             |
|                   | WaveSideBar                          | 类似于通讯录带字母选择的list           | [https://gitee.com/HarmonyOS-TPC/WaveSideBar](https://gitee.com/HarmonyOS-TPC/WaveSideBar)                               |
|                   | SwipeActionAdapter                   | list侧滑菜单                   | [https://gitee.com/HarmonyOS-TPC/SwipeActionAdapter](https://gitee.com/HarmonyOS-TPC/SwipeActionAdapter)                 |
| 6.5 PageSlider    | ViewPagerIndicator                   | 星级最高的Slider组件              | [https://gitee.com/HarmonyOS-TPC/ViewPagerIndicator](https://gitee.com/HarmonyOS-TPC/ViewPagerIndicator)                   |
|                   | PageIndicatorView                    | 自定义适配器组件                   | [https://gitee.com/HarmonyOS-TPC/PageIndicatorView](https://gitee.com/HarmonyOS-TPC/PageIndicatorView)                     |
|                   | UltraViewPager                       | 多种样式的Slider自定义控件           | [https://gitee.com/HarmonyOS-TPC/UltraViewPager](https://gitee.com/HarmonyOS-TPC/UltraViewPager)                           |
|                   | SlidingDrawer                        | 自定义Slider组件                | [https://gitee.com/HarmonyOS-TPC/SlidingDrawer](https://gitee.com/HarmonyOS-TPC/SlidingDrawer)                             |
|                   | AppIntro                             | 各种转场动画集合                   | [https://gitee.com/HarmonyOS-TPC/AppIntro](https://gitee.com/HarmonyOS-TPC/AppIntro)                                     |
|                   | ParallaxViewPager                    | 自定义Slider组件                | [https://gitee.com/HarmonyOS-TPC/ParallaxViewPager](https://gitee.com/HarmonyOS-TPC/ParallaxViewPager)                   |
|                   | MZBannerView                         | 仿魅族BannerView自定义组件         | [https://gitee.com/HarmonyOS-TPC/MZBannerView](https://gitee.com/HarmonyOS-TPC/MZBannerView)                             |
|                   | FlycoPageIndicator                   | 多种样式的适配器组件                 | [https://gitee.com/HarmonyOS-TPC/FlycoPageIndicator](https://gitee.com/HarmonyOS-TPC/FlycoPageIndicator)                 |
|                   | SCViewPager                          | 不规则淡入淡出的Slider             | [https://gitee.com/HarmonyOS-TPC/SCViewPager](https://gitee.com/HarmonyOS-TPC/SCViewPager)                               |
|                   | ImageCoverFlow                       | 3D视角适配器                    | [https://gitee.com/HarmonyOS-TPC/ImageCoverFlow](https://gitee.com/HarmonyOS-TPC/ImageCoverFlow)                           |
| 6.6 ProgressBar   | MaterialProgressBar                  | 多种样式自定义progressbar         | [https://gitee.com/HarmonyOS-TPC/MaterialProgressBar](https://gitee.com/HarmonyOS-TPC/MaterialProgressBar)               |
|                   | discreteSeekBar                      | 冒泡式显示自定义seekbar            | [https://gitee.com/HarmonyOS-TPC/discreteSeekBar](https://gitee.com/HarmonyOS-TPC/discreteSeekBar)                       |
|                   | materialish-progress                 | 自定义样式的progressbar          | [https://gitee.com/HarmonyOS-TPC/materialish-progress](https://gitee.com/HarmonyOS-TPC/materialish-progress)           |
|                   | ohos-HoloCircularProgressBar         | 自定义progressBar             | [https://gitee.com/HarmonyOS-TPC/ohos-HoloCircularProgressBar](https://gitee.com/HarmonyOS-TPC/ohos-HoloCircularProgressBar) |
|                   | circular-music-progressbar           | 类似于音乐播放器的圆形progressbar     | [https://gitee.com/HarmonyOS-TPC/circular-music-progressbar](https://gitee.com/HarmonyOS-TPC/circular-music-progressbar) |
|                   | SectorProgressView                   | 自定义圆形progressBar           | [https://gitee.com/HarmonyOS-TPC/SectorProgressView](https://gitee.com/HarmonyOS-TPC/SectorProgressView)               |
|                   | LikeSinaSportProgress                | 类似于新浪的两边比拼进度条              | [https://gitee.com/HarmonyOS-TPC/LikeSinaSportProgress](https://gitee.com/HarmonyOS-TPC/LikeSinaSportProgress)         |
| 6.7 Dialog/弹出框    | sweet-alert-dialog                   | 自定义对话框                     | [https://gitee.com/HarmonyOS-TPC/sweet-alert-dialog](https://gitee.com/HarmonyOS-TPC/sweet-alert-dialog)                 |
|                   | LovelyDialog                         | 自定义样式的Dialog               | [https://gitee.com/HarmonyOS-TPC/LovelyDialog](https://gitee.com/HarmonyOS-TPC/LovelyDialog)                           |
|                   | CookieBar                            | 顶部底部弹出的自定义对话框              | [https://gitee.com/HarmonyOS-TPC/CookieBar](https://gitee.com/HarmonyOS-TPC/CookieBar)                                 |
|                   | Alerter                              | 顶部提示组件                     | [https://gitee.com/HarmonyOS-TPC/Alerter](https://gitee.com/HarmonyOS-TPC/Alerter)                                     |
|                   | StatusView                           | 顶部弹出的状态视图                  | [https://gitee.com/HarmonyOS-TPC/StatusView](https://gitee.com/HarmonyOS-TPC/StatusView)                               |
| 6.8 Layout        | vlayout                              | 可以嵌套列表布局                   | [https://gitee.com/HarmonyOS-TPC/vlayout](https://gitee.com/HarmonyOS-TPC/vlayout)               |
|                   | flexbox-layout                       | 按照百分比控制的布局                 | [https://gitee.com/HarmonyOS-TPC/flexbox-layout](https://gitee.com/HarmonyOS-TPC/flexbox-layout)        |
|                   | ohosAutoLayout                       | 根据特定效果图尺寸，按比例自适应布局         | [https://gitee.com/HarmonyOS-TPC/ohosAutoLayout](https://gitee.com/HarmonyOS-TPC/ohosAutoLayout)        |
|                   | yoga                                 | facebook基于flexbox的布局引擎     | [https://gitee.com/HarmonyOS-TPC/yoga](https://gitee.com/HarmonyOS-TPC/yoga)                  |
| 6.9 Tab/菜单切换      | FlycoTabLayout                       | 自定义TabLayout组件             | [https://gitee.com/HarmonyOS-TPC/FlycoTabLayout](https://gitee.com/HarmonyOS-TPC/FlycoTabLayout)        |
|                   | NavigationTabBar                     | 各种样式TabBar合集               | [https://gitee.com/HarmonyOS-TPC/NavigationTabBar](https://gitee.com/HarmonyOS-TPC/NavigationTabBar)      |
|                   | BottomBar                            | 自定义底部菜单栏                   | [https://gitee.com/HarmonyOS-TPC/BottomBar](https://gitee.com/HarmonyOS-TPC/BottomBar)             |
|                   | BottomNavigation                     | 多种样式自定义底部菜单栏               | [https://gitee.com/HarmonyOS-TPC/BottomNavigation](https://gitee.com/HarmonyOS-TPC/BottomNavigation)      |
|                   | ahbottomnavigation                   | 多种样式自定义底部菜单栏               | [https://gitee.com/HarmonyOS-TPC/ahbottomnavigation](https://gitee.com/HarmonyOS-TPC/ahbottomnavigation)    |
|                   | HorizontalPicker                     | 横向菜单选择器                    | [https://gitee.com/HarmonyOS-TPC/HorizontalPicker](https://gitee.com/HarmonyOS-TPC/HorizontalPicker)      |
|                   | StatefulLayout                       | 可以左右切换布局有点类似PageSlider     | [https://gitee.com/HarmonyOS-TPC/StatefulLayout](https://gitee.com/HarmonyOS-TPC/StatefulLayout)        |
| 6.10 Toast        | Toasty                               | 简单好用的Toast调用工具             | [https://github.com/GrenderG/Toasty](https://github.com/GrenderG/Toasty)                      |
|                   | FancyToast-ohos                      | Toast调用封装工具                | [https://gitee.com/HarmonyOS-TPC/FancyToast-ohos](https://gitee.com/HarmonyOS-TPC/FancyToast-ohos)       |
| 6.11 Time/Date    | ohos-times-square                    | 简单的日历组件                    | [https://gitee.com/HarmonyOS-TPC/ohos-times-square](https://gitee.com/HarmonyOS-TPC/ohos-times-square)     |
|                   | CountdownView                        | 多种效果的时间计时器                 | [https://gitee.com/HarmonyOS-TPC/CountdownView](https://gitee.com/HarmonyOS-TPC/CountdownView)         |
| 6.12 其他           | BGARefreshLayout-ohos                | 基于多个场景的下拉刷新                | [https://gitee.com/HarmonyOS-TPC/BGARefreshLayout-ohos](https://gitee.com/HarmonyOS-TPC/BGARefreshLayout-ohos) |
|                   | ohos-Bootstrap                       | 多种自定义控件合集                  | [https://gitee.com/HarmonyOS-TPC/ohos-Bootstrap](https://gitee.com/HarmonyOS-TPC/ohos-Bootstrap)        |
|                   | ohosSlidingUpPanel                   | 底部上滑布局                     | [https://gitee.com/HarmonyOS-TPC/ohosSlidingUpPanel](https://gitee.com/HarmonyOS-TPC/ohosSlidingUpPanel)    |
|                   | Fragmentation                        | 侧边菜单                       | [https://gitee.com/HarmonyOS-TPC/Fragmentation](https://gitee.com/HarmonyOS-TPC/Fragmentation)         |
|                   | triangle-view                        | 三角图                        | [https://gitee.com/HarmonyOS-TPC/triangle-view](https://gitee.com/HarmonyOS-TPC/triangle-view)         |
|                   | MaterialDesignLibrary                | 基于MaterialDesign的各种自定义控件合集 | [https://gitee.com/HarmonyOS-TPC/MaterialDesignLibrary](https://gitee.com/HarmonyOS-TPC/MaterialDesignLibrary) |
|                   | XPopup                               | 包含dialog，图片加载等各式自定义控件      | [https://gitee.com/HarmonyOS-TPC/Xpopup](https://gitee.com/HarmonyOS-TPC/Xpopup)                |
|                   | cardslib                             | 卡片式布局库                     | [https://gitee.com/HarmonyOS-TPC/cardslib](https://gitee.com/HarmonyOS-TPC/cardslib)              |
|                   | Swipecards                           | 滑动卡片组件                     | [https://gitee.com/HarmonyOS-TPC/Swipecards](https://gitee.com/HarmonyOS-TPC/Swipecards)            |
|                   | SlideUp-ohos                         | 从下方滑动出来的布局控件               | [https://gitee.com/HarmonyOS-TPC/SlideUp-ohos](https://gitee.com/HarmonyOS-TPC/SlideUp-ohos)          |
|                   | EazeGraph                            | 柱状图圆形图山峰图                  | [https://gitee.com/HarmonyOS-TPC/EazeGraph](https://gitee.com/HarmonyOS-TPC/EazeGraph)             |
|                   | WheelView                            | 轮盘选择                       | [https://gitee.com/HarmonyOS-TPC/WheelView](https://gitee.com/HarmonyOS-TPC/WheelView)             |
|                   | RulerView                            | 卷尺控件                       | [https://gitee.com/HarmonyOS-TPC/RulerView](https://gitee.com/HarmonyOS-TPC/RulerView)             |
|                   | MultiCardMenu                        | 底部弹出的自定义菜单集合               | [https://gitee.com/HarmonyOS-TPC/MultiCardMenu](https://gitee.com/HarmonyOS-TPC/MultiCardMenu)         |
|                   | DividerDrawable                      | 分割线绘制                      | [https://gitee.com/HarmonyOS-TPC/DividerDrawable](https://gitee.com/HarmonyOS-TPC/DividerDrawable)       |
|                   | ProtractorView                       | 量角器控件                      | [https://gitee.com/HarmonyOS-TPC/ProtractorView](https://gitee.com/HarmonyOS-TPC/ProtractorView)        |
|                   | ohos-ExpandIcon                      | 箭头控件                       | [https://gitee.com/HarmonyOS-TPC/ohos-ExpandIcon](https://gitee.com/HarmonyOS-TPC/ohos-ExpandIcon)       |
|                   | GestureLock                          | 可自定义配置的手势动画解锁              | [https://gitee.com/HarmonyOS-TPC/GestureLock](https://gitee.com/HarmonyOS-TPC/GestureLock)           |
|                   | williamchart                         | 柱状图圆形图进度图山峰图               | [https://gitee.com/HarmonyOS-TPC/williamchart](https://gitee.com/HarmonyOS-TPC/williamchart)          |
|                   | labelview                            | 自定义角标图                     | [https://gitee.com/HarmonyOS-TPC/labelview](https://gitee.com/HarmonyOS-TPC/labelview)             |
|                   | PatternLockView                      | 简单的手势解锁                    | [https://gitee.com/HarmonyOS-TPC/PatternLockView](https://gitee.com/HarmonyOS-TPC/PatternLockView)       |
|                   | BadgeView                            | 图标的标签图                     | [https://gitee.com/HarmonyOS-TPC/BadgeView](https://gitee.com/HarmonyOS-TPC/BadgeView)             |
|                   | MaterialBadgeTextView                | 图标的标签图                     | [https://gitee.com/HarmonyOS-TPC/MaterialBadgeTextView](https://gitee.com/HarmonyOS-TPC/MaterialBadgeTextView) |
|                   | SlantedTextView                      | 自定义角标图                     | [https://gitee.com/HarmonyOS-TPC/SlantedTextView](https://gitee.com/HarmonyOS-TPC/SlantedTextView)       |
|                   | TriangleLabelView                    | 三角形角标图                     | [https://gitee.com/HarmonyOS-TPC/TriangleLabelView](https://gitee.com/HarmonyOS-TPC/TriangleLabelView)     |
|                   | GoodView                             | 带特效点赞按钮                    | [https://gitee.com/HarmonyOS-TPC/GoodView](https://gitee.com/HarmonyOS-TPC/GoodView)              |
|                   | StateViews                           | 自定义状态提示控件                  | [https://gitee.com/HarmonyOS-TPC/StateViews](https://gitee.com/HarmonyOS-TPC/StateViews)            |
|                   | WaveView                             | 自定义水平面样式控件                 | [https://gitee.com/HarmonyOS-TPC/WaveView](https://gitee.com/HarmonyOS-TPC/WaveView)              |
|                   | CircleRefreshLayout                  | 下拉刷新组件                     | [https://gitee.com/HarmonyOS-TPC/CircleRefreshLayout](https://gitee.com/HarmonyOS-TPC/CircleRefreshLayout)   |


### 七、框架类

|名称             | 介绍                                          | 资源地址                                           |
|--------------- |---------------------------------------------|------------------------------------------------|
|TheMVP          | mvp框架                                       | [https://gitee.com/HarmonyOS-TPC/TheMVP](https://gitee.com/HarmonyOS-TPC/TheMVP)         |
|ohos-ZBLibrary  | MVP框架，同时附有OKhttp，glide，zxing等常用工具    | [https://gitee.com/HarmonyOS-TPC/ohos-ZBLibrary](https://gitee.com/HarmonyOS-TPC/ohos-ZBLibrary) |

